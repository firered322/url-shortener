const express = require('express');
const router = express.Router();

const Url = require('../models/Url');

router.get("/", (req, res) => res.send("home route"));

// @route     GET /:code
// @desc      Redirect to long/original URL
router.get('/:code', async (req, res) => {
  try {
  
    const url = await Url.findOne({ urlCode: req.params.code });

    console.log("Lets Play: ", req.ip);

    if (url) {
    //   url.clicks += 1;
      await url.save();
	//   return res.redirect(url.longUrl);
	return res.send(url)
    } else {
      return res.status(404).json('No url found');
    }
  } catch (err) {
    console.error(err);
    res.status(500).json('Server error');
  }
});

module.exports = router;
