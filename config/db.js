const mongoose = require('mongoose');

const connectionStr = `mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}/dev?authSource=admin`
const connectDB = async () => {
    try {
        await mongoose.connect(connectionStr, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });
        console.log("MongoDB connected");
    } catch (err) {
		console.log('mongo connection error')
        console.error(err.message);
        process.exit(1);
    }
};

module.exports = connectDB;
